# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
DOCKER ?= docker
PIP ?= $(PYTHON) -m pip
PIP_COMPILE ?= $(VENV)/bin/pip-compile
IMAGE ?= api-proxy

BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)

.PHONY: all
all: serve

.PHONY: setup
setup: $(VENV)/requirements.txt

requirements.txt: requirements.in | $(VENV)
	$(PIP_COMPILE) --no-emit-index-url --upgrade

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	$(PYTHON) -m pip install -r requirements.txt
	cp -f requirements.txt $(VENV)/requirements.txt

$(VENV):
	python3 -m venv $(VENV)
	$(PYTHON) -m pip install --upgrade pip setuptools wheel pip-tools

.PHONY: clean
clean:
	find -name __pycache__ -type d -exec rm -rf '{}' \;
	find -name \*.pyc -type f -exec rm -f '{}' \;

.PHONY: distclean
distclean: clean
	rm -rf $(VENV)

.PHONY: serve
serve: setup
	$(PYTHON) app.py

.PHONY: docker-build
docker-build:
	$(DOCKER) build -t $(IMAGE) .

.PHONY: docker-run
docker-run:
	$(DOCKER) run --rm -it $(IMAGE)

.PHONY: docker-run-bash
docker-run-bash:
	$(DOCKER) run --rm -it $(IMAGE) bash
